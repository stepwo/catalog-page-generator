FROM java:openjdk-8-jdk

MAINTAINER Frederik Hahne <atomfrede@gmail.com>

EXPOSE 3434

ADD config_prod.yml config.yml
ADD ./target/catalog-page-generator-*.jar catalog-page-generator.jar

CMD java -jar catalog-page-generator.jar server config.yml