# Catalog Generator

A simple dropwizard application that generates XML files to make creation of holiday catalogs more easy.

[![build status](https://gitlab.com/ci/projects/6991/status.png?ref=master)](https://gitlab.com/ci/projects/6991?ref=master)
[![codecov.io](http://codecov.io/gitlab/stepwo/catalog-page-generator/coverage.svg?branch=master)](http://codecov.io/gitlab/stepwo/catalog-page-generator?branch=master)
[![Dependency Status](https://www.versioneye.com/user/projects/55e92906d7aa5400100001ad/badge.svg?style=flat)](https://www.versioneye.com/user/projects/55e92906d7aa5400100001ad)

## Requirements

* [JDK8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
* [Maven 3.2](http://maven.apache.org/)
* [git](http://git-scm.com/)

## Setup and Run

* ``git git@gitlab.com:stepwo/catalog-page-generator.git``
* ``mvn package``
* ``java -jar target/catalog-page-generator-0.1-SNAPSHOT.jar server``
* Goto ``http://localhost:8080/catalogPage`` to generate a xml

### Alternative

* Run the main class in ``CatalogPageGeneratorApplication`` directly from your IDE

## Production

* ``java -jar target/catalog-page-generator-0.1-SNAPSHOT.jar server your-config.yml``


## License

    Copyright Frederik Hahne, Andreas Herting

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.