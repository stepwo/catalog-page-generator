package de.atomfrede.gitlab.catalog.page.generator.entity;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class HouseTest {

    @Test
    public void assertHouseCanBeJsonSerialized() throws JsonProcessingException {

        House house = new House()
                .setName("Typ A")
                .setPersonCount("3 Pers.*")
                .addPrice("Saison 1", "300€")
                .addPrice("Saison 2", "450€");


        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(house);

        System.out.println(json);
        assertThat(json).isEqualTo("{\"name\":\"Typ A\",\"personCount\":\"3 Pers.*\",\"prices\":[{\"season\":\"Saison 1\",\"price\":\"300€\"},{\"season\":\"Saison 2\",\"price\":\"450€\"}]}");

    }

    @Test
    public void assertThatHouseCanSerializedToHeader() {

        House house = new House()
                .setName("Typ A")
                .setPersonCount("3 Pers.*")
                .addPrice("Saison 1", "300€")
                .addPrice("Saison 2", "450€");

        String header = house.getHeader();

        assertThat(header).isEqualTo("Wohnungstyp||Saison 1|Saison 2\n");
    }

    @Test
    public void assertThatHouseCanBeSerializedToPriceLine() {

        House house = new House()
                .setName("Typ A")
                .setPersonCount("3 Pers.")
                .addPrice("Saison 1", "300€")
                .addPrice("Saison 2", "450€");

        String line = house.getPrices();

        assertThat(line).isEqualTo("Typ A|3 Pers.|300€|450€\n");
    }
}
