package de.atomfrede.gitlab.catalog.page.generator;

import de.atomfrede.gitlab.catalog.page.generator.config.CatalogGeneratorConfig;
import de.atomfrede.gitlab.catalog.page.generator.resource.CatalogPageResource;
import io.dropwizard.jersey.setup.JerseyEnvironment;
import io.dropwizard.lifecycle.setup.LifecycleEnvironment;
import io.dropwizard.setup.Environment;
import mockit.Mocked;
import mockit.NonStrictExpectations;
import mockit.Verifications;
import mockit.integration.junit4.JMockit;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.templateresolver.ITemplateResolver;

@RunWith(JMockit.class)
public class CatalogPageGeneratorApplicationTest {

    @Mocked
    Environment environment;
    @Mocked
    JerseyEnvironment jersey;
    @Mocked
    LifecycleEnvironment lifecycle;
    @Mocked
    TemplateEngine templateEngine;

    CatalogPageGeneratorApplication application = new CatalogPageGeneratorApplication();
    CatalogGeneratorConfig configuration = new CatalogGeneratorConfig();

    @Before
    public void setup() {

        new NonStrictExpectations() {{

            environment.jersey();
            result = jersey;

            environment.lifecycle();
            result = lifecycle;

        }};
    }

    @Test
    public void shouldAddCatalogPageResource() throws Exception {

        application.run(configuration, environment);

        new Verifications() {{

            jersey.register(withInstanceOf(CatalogPageResource.class));
        }};
    }

    @Test
    public void shouldInitTemplateEngine() throws Exception {

        application.run(configuration, environment);

        new Verifications() {{

            templateEngine.setTemplateResolver((ITemplateResolver) any);
        }};
    }
}
