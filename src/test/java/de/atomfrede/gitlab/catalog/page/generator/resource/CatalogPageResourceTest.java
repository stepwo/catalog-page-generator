package de.atomfrede.gitlab.catalog.page.generator.resource;


import com.fasterxml.jackson.databind.ObjectMapper;
import de.atomfrede.gitlab.catalog.page.generator.config.CatalogGeneratorConfig;
import de.atomfrede.gitlab.catalog.page.generator.entity.Page;
import de.atomfrede.gitlab.catalog.page.generator.generator.XMLGenerator;
import org.junit.Before;
import org.junit.Test;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;

import java.io.IOException;

public class CatalogPageResourceTest {

    TemplateEngine templateEngine;
    XMLGenerator generator;

    CatalogPageResource resource;

    @Before
    public void setup() {

        ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
        templateResolver.setCharacterEncoding("UTF-8");
        templateResolver.setTemplateMode("XML");
        templateResolver.setSuffix(".xml");
        templateResolver.setPrefix("templates/");

        templateEngine = new TemplateEngine();
        templateEngine.setTemplateResolver(templateResolver);

        generator = new XMLGenerator(templateEngine, new CatalogGeneratorConfig());

        resource = new CatalogPageResource(generator);
    }

    @Test
    public void assertThatPageIsConvertedToXml() throws IOException {

        Page page = new Page()
                .setNumber(22)
                .setSlogan("Fuel the fight: Drink coffee!");

        ObjectMapper mapper = new ObjectMapper();
        String pageJson = mapper.writeValueAsString(page);

        resource.generateXml(pageJson);
    }
}
