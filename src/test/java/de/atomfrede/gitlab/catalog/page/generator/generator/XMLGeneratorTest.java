package de.atomfrede.gitlab.catalog.page.generator.generator;

import de.atomfrede.gitlab.catalog.page.generator.config.CatalogGeneratorConfig;
import de.atomfrede.gitlab.catalog.page.generator.entity.House;
import de.atomfrede.gitlab.catalog.page.generator.entity.Page;
import de.atomfrede.gitlab.catalog.page.generator.entity.Season;
import org.junit.Before;
import org.junit.Test;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class XMLGeneratorTest {

    TemplateEngine templateEngine;
    XMLGenerator generator;

    @Before
    public void setup() {

        ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
        templateResolver.setCharacterEncoding("UTF-8");
        templateResolver.setTemplateMode("XML");
        templateResolver.setSuffix(".xml");
        templateResolver.setPrefix("templates/");

        templateEngine = new TemplateEngine();
        templateEngine.setTemplateResolver(templateResolver);

        generator = new XMLGenerator(templateEngine, new CatalogGeneratorConfig());
    }

    @Test
    public void assertThatAllVariablesAreGenerated() {

        String generatedXml = generator.createPage(new Page()
                .setNumber(42)
                .setTitle("My Title")
                .setRegion("My Region")
                .setSlogan("Don't be evil!")
                .setHouse("Ein wirklich tolles Haus!!!"));

        assertThat(generatedXml).contains("<Nummer>42</Nummer>");
        assertThat(generatedXml).contains("<Quartiertitel>My Title</Quartiertitel>");
        assertThat(generatedXml).contains("<Quartierregion>My Region</Quartierregion>");
        assertThat(generatedXml).contains("<Quartierslogan>Don&#39;t be evil!</Quartierslogan>");

        assertThat(generatedXml).contains("<HausUndRegion_Text>Ein wirklich tolles Haus!!!</HausUndRegion_Text>");
    }

    @Test
    public void assertThatComplexObjectsAreGenerated() {

        Season seasonOne = new Season();
        Season seasonTwo = new Season();

        seasonOne
                .setName("Saison 1")
                .addSubSeason("Frühling", "26.04. - 24.05")
                .addSubSeason("Herbst", "06.09. - 11.10");

        seasonTwo
                .setName("Saison 2")
                .addSubSeason("Frühling", "26.04. - 24.05")
                .addSubSeason("Herbst", "06.09. - 11.10");



        House house = new House()
                .setName("Typ A")
                .setPersonCount("3 Pers.*")
                .addPrice("Saison 1", "300€")
                .addPrice("Saison 2", "450€");

        List<House> houses = new ArrayList<>();
        houses.add(house);

        List<Season> seasons = new ArrayList<>();
        seasons.add(seasonOne);
        seasons.add(seasonTwo);

        String generatedXml = generator.createPage(new Page()
                .setNumber(42)
                .setTitle("My Title")
                .setRegion("My Region")
                .setSlogan("Don't be evil!")
                .setHouse("Ein wirklich tolles Haus!!!")
                .setHouses(houses)
                .setSeasons(seasons));
        
        assertThat(generatedXml).contains(" <Preise_Tabelle>Wohnungstyp||Saison 1|Saison 2\n" +
                "Typ A|3 Pers.*|300€|450€\n" +
                "</Preise_Tabelle>");
        assertThat(generatedXml).contains("<Saisonzeiten_Tabelle>Saison 1|Frühling|26.04. - 24.05\n" +
                "|Herbst|06.09. - 11.10\n" +
                "Saison 2|Frühling|26.04. - 24.05\n" +
                "|Herbst|06.09. - 11.10\n" +
                "</Saisonzeiten_Tabelle>");
    }
}
