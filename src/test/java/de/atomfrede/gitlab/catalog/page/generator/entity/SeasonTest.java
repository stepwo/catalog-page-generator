package de.atomfrede.gitlab.catalog.page.generator.entity;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class SeasonTest {

    @Test
    public void assertSeasonCanBeJsonSerialized() throws JsonProcessingException {

        Season seasonOne = new Season();

        seasonOne
                .setName("Saison 1")
                .addSubSeason("Frühling", "26.04. - 24.05")
                .addSubSeason("Herbst", "06.09. - 11.10");


        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(seasonOne);

        assertThat(json).isEqualTo("{\"name\":\"Saison 1\",\"subSeasons\":[{\"name\":\"Frühling\",\"date\":\"26.04. - 24.05\"},{\"name\":\"Herbst\",\"date\":\"06.09. - 11.10\"}]}");

    }

    @Test
    public void assertThatSeasonsCanBeSerializedToXMLContent() {

        Season seasonOne = new Season();

        seasonOne
                .setName("Saison 1")
                .addSubSeason("Frühling", "26.04. - 24.05")
                .addSubSeason("Herbst", "06.09. - 11.10");

        String xmlContent = seasonOne.toXmlContent();

        assertThat(xmlContent).isEqualTo("Saison 1|Frühling|26.04. - 24.05\n" +
                "|Herbst|06.09. - 11.10");
    }
}
