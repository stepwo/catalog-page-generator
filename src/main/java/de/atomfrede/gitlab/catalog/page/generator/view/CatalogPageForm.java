package de.atomfrede.gitlab.catalog.page.generator.view;

import io.dropwizard.views.View;

import java.nio.charset.Charset;

public class CatalogPageForm extends View {

    public CatalogPageForm() {
        super("catalogPageView.mustache", Charset.forName("UTF-8"));
    }
}
