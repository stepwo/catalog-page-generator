package de.atomfrede.gitlab.catalog.page.generator.generator;

import de.atomfrede.gitlab.catalog.page.generator.config.CatalogGeneratorConfig;
import de.atomfrede.gitlab.catalog.page.generator.entity.House;
import de.atomfrede.gitlab.catalog.page.generator.entity.Page;
import de.atomfrede.gitlab.catalog.page.generator.entity.Season;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.util.Iterator;
import java.util.Locale;

public class XMLGenerator {

    private TemplateEngine templateEngine;
    private CatalogGeneratorConfig config;

    public XMLGenerator(TemplateEngine templateEngine, CatalogGeneratorConfig config) {

        this.templateEngine = templateEngine;
        this.config = config;
    }

    public String createPage(Page page) {

        Context ctx = new Context(Locale.GERMANY);
        ctx.setVariable("number", page.number());
        ctx.setVariable("title", page.title());
        ctx.setVariable("region", page.region());
        ctx.setVariable("slogan", page.slogan());

        ctx.setVariable("house", page.house());

        ctx.setVariable("forBabys", page.forBabys());
        ctx.setVariable("babysExtend", page.babysExtend());
        ctx.setVariable("babysPeriod", page.babysPeriod());

        ctx.setVariable("forToddlers", page.forToddlers());
        ctx.setVariable("toddlersExtend", page.toddlersExtend());
        ctx.setVariable("toddlersPeriod", page.toddlersPeriod());

        ctx.setVariable("forPreschool", page.forPreschool());
        ctx.setVariable("preschoolExtend", page.preschoolExtend());
        ctx.setVariable("preschoolPeriod", page.preschoolPeriod());

        ctx.setVariable("forChildren", page.forChildren());
        ctx.setVariable("childrenExtend", page.childrenExtend());
        ctx.setVariable("childrenPeriod", page.childrenPeriod());

        ctx.setVariable("forTeenagers", page.forTeenagers());
        ctx.setVariable("teenagerExtend", page.teenagerExtend());
        ctx.setVariable("teenagerPeriod", page.teenagerPeriod());

        ctx.setVariable("nearby", page.nearby());

        ctx.setVariable("foodAndBeverage", page.foodAndBeverage());
        ctx.setVariable("sourroundings", page.sourroundings());
        ctx.setVariable("sport", page.sport());
        ctx.setVariable("pets", page.pets());
        ctx.setVariable("travel", page.travel());

        ctx.setVariable("priceStar", page.priceStar());
        ctx.setVariable("priceIncluded", page.priceIncluded());
        ctx.setVariable("minimumStay", page.minimumStay());
        ctx.setVariable("careOffers", page.careOffers());

        if (page.seasons() != null && !page.seasons().isEmpty()) {
            ctx.setVariable("seasons", getSeasons(page));
        }

        if (page.houses() != null && !page.houses().isEmpty()) {
            ctx.setVariable("houses", getHouses(page));
        }


        if (config.useProductionXML) {
            return templateEngine.process("quartier_production", ctx);
        } else {
            return templateEngine.process("quartier", ctx);
        }


    }

    private String getSeasons(Page page) {

        StringBuilder sb = new StringBuilder();

        Iterator<Season> seasons = page.seasons().iterator();
        while (seasons.hasNext()) {
            sb.append(seasons.next().toXmlContent());
            sb.append("\n");
        }

        return sb.toString();
    }

    private String getHouses(Page page) {

        StringBuilder sb = new StringBuilder();

        sb.append(page.houses().get(0).getHeader());

        Iterator<House> houses = page.houses().iterator();
        while (houses.hasNext()) {
            sb.append(houses.next().getPrices());
        }
        return sb.toString();
    }
}
