package de.atomfrede.gitlab.catalog.page.generator.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class Season {

    @JsonProperty
    private String name;
    @JsonProperty
    private List<SubSeason> subSeasons;

    public Season addSubSeason(String subname, String date) {

        if (subSeasons == null) {
            subSeasons = new ArrayList<>();
        }

        SubSeason subSeason = new SubSeason()
                .setName(subname)
                .setDate(date);
        subSeasons.add(subSeason);

        return this;
    }

    public String name() {
        return this.name;
    }

    public Season setName(final String name) {
        this.name = name;
        return this;
    }

    @JsonIgnore
    public String toXmlContent() {


        StringBuilder content = new StringBuilder();
        content.append(name.trim());

        if (!subSeasons.isEmpty()) {
            SubSeason firstSubSeason = subSeasons.get(0);

            content.append("|");
            content.append(firstSubSeason.name());
            content.append("|");
            content.append(firstSubSeason.date());

            for (int i = 1; i < subSeasons.size(); i++) {

                SubSeason subSeason = subSeasons.get(i);
                content.append("\n|");
                content.append(subSeason.name());
                content.append("|");
                content.append(subSeason.date());
            }
        }


        return content.toString();

    }


}
