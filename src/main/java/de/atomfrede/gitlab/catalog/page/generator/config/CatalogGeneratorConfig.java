package de.atomfrede.gitlab.catalog.page.generator.config;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.ImmutableMap;
import io.dropwizard.Configuration;

import javax.validation.constraints.NotNull;
import java.util.Map;

public class CatalogGeneratorConfig extends Configuration {

    @JsonProperty
    public Boolean useProductionXML = false;

    @NotNull
    private ImmutableMap<String, ImmutableMap<String, String>> viewRendererConfiguration = ImmutableMap
            .of();

    // unfortunate change in Dropwizard 0.8 make this complex view configuration
    // object necessary. Hopefully fixed in next release
    @JsonProperty("viewRendererConfiguration")
    public ImmutableMap<String, ImmutableMap<String, String>> getViewRendererConfiguration() {
        return viewRendererConfiguration;
    }

    // unfortunate change in Dropwizard 0.8 make this complex view configuration
    // object necessary. Hopefully fixed in next release
    @JsonProperty("viewRendererConfiguration")
    public void setViewRendererConfiguration(
            Map<String, Map<String, String>> viewRendererConfiguration) {
        ImmutableMap.Builder<String, ImmutableMap<String, String>> builder = ImmutableMap
                .builder();
        for (Map.Entry<String, Map<String, String>> entry : viewRendererConfiguration
                .entrySet()) {
            builder.put(entry.getKey(), ImmutableMap.copyOf(entry.getValue()));
        }
        this.viewRendererConfiguration = builder.build();
    }

}
