package de.atomfrede.gitlab.catalog.page.generator;

import de.atomfrede.gitlab.catalog.page.generator.config.CatalogGeneratorConfig;
import de.atomfrede.gitlab.catalog.page.generator.generator.XMLGenerator;
import de.atomfrede.gitlab.catalog.page.generator.resource.CatalogPageResource;
import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.configuration.EnvironmentVariableSubstitutor;
import io.dropwizard.configuration.SubstitutingSourceProvider;
import io.dropwizard.java8.Java8Bundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.dropwizard.views.ViewBundle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;

public class CatalogPageGeneratorApplication extends Application<CatalogGeneratorConfig> {

    private static final Logger LOGGER = LoggerFactory.getLogger(CatalogPageGeneratorApplication.class);

    TemplateEngine templateEngine;

    public static void main(final String[] args) throws Exception {
        new CatalogPageGeneratorApplication().run(args);
    }

    @Override
    public void initialize(final Bootstrap<CatalogGeneratorConfig> bootstrap) {

        bootstrap.setConfigurationSourceProvider(
                new SubstitutingSourceProvider(bootstrap.getConfigurationSourceProvider(), new EnvironmentVariableSubstitutor(false))
        );

        bootstrap.addBundle(new AssetsBundle("/assets/"));

        bootstrap.addBundle(new ViewBundle<CatalogGeneratorConfig>());
        bootstrap.addBundle(new Java8Bundle());
    }


    @Override
    public void run(CatalogGeneratorConfig configuration, Environment environment) throws Exception {

        LOGGER.info("Starting catalog page application");

        initializeTemplateEngine();

        CatalogPageResource catalogPageResource = new CatalogPageResource(new XMLGenerator(templateEngine, configuration));

        environment.jersey().register(catalogPageResource);
    }

    public void initializeTemplateEngine() {

        ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
        templateResolver.setCharacterEncoding("UTF-8");
        templateResolver.setTemplateMode("XML");
        templateResolver.setSuffix(".xml");
        templateResolver.setPrefix("templates/");

        templateEngine = new TemplateEngine();
        templateEngine.setTemplateResolver(templateResolver);
    }
}
