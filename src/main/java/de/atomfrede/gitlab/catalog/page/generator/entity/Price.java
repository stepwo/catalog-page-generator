package de.atomfrede.gitlab.catalog.page.generator.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Price {

    @JsonProperty
    private String season;
    @JsonProperty
    private String price;

    public String season() {
        return this.season;
    }

    public String price() {
        return this.price;
    }

    public Price setSeason(final String season) {
        this.season = season;
        return this;
    }

    public Price setPrice(final String price) {
        this.price = price;
        return this;
    }


}
