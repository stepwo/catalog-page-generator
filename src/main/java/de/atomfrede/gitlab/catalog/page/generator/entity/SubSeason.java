package de.atomfrede.gitlab.catalog.page.generator.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SubSeason {

    @JsonProperty
    private String name;
    @JsonProperty
    private String date;

    public String name() {
        return this.name;
    }

    public String date() {
        return this.date;
    }

    public SubSeason setName(final String name) {
        this.name = name;
        return this;
    }

    public SubSeason setDate(final String date) {
        this.date = date;
        return this;
    }


}
