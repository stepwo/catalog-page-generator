package de.atomfrede.gitlab.catalog.page.generator.resource;

import com.codahale.metrics.annotation.Timed;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.atomfrede.gitlab.catalog.page.generator.entity.Page;
import de.atomfrede.gitlab.catalog.page.generator.generator.XMLGenerator;
import de.atomfrede.gitlab.catalog.page.generator.view.CatalogPageForm;
import org.apache.commons.io.FileUtils;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.File;
import java.io.IOException;

@Path("/catalogPage")
public class CatalogPageResource {

    private XMLGenerator xmlGenerator;
    private ObjectMapper mapper;

    public CatalogPageResource(XMLGenerator xmlGenerator) {

        this.mapper = new ObjectMapper();
        this.xmlGenerator = xmlGenerator;
    }

    @GET
    @Produces(MediaType.TEXT_HTML)
    public CatalogPageForm getCatalogPageForm() {
        return new CatalogPageForm();
    }

    @POST
    @Timed
    @Path("/generate")
    public Response generateXml(@FormParam("page") String page) throws IOException {

        try {

            Page realPage = toPage(page);

            return Response.ok(generateXMLFile(realPage))
                    .header("Content-Disposition", "attachment; filename=" + realPage.number() + ".xml")
                    .build();

        } catch (Exception e) {
            throw new WebApplicationException();
        }

    }


    private Page toPage(String json) throws IOException {

        return mapper.readValue(json, Page.class);
    }

    private File generateXMLFile(Page p) throws IOException {
        String xml = xmlGenerator.createPage(p);
        File f = File.createTempFile("page", ".xml");
        FileUtils.write(f, xml);

        return f;
    }


}
