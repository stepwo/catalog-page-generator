package de.atomfrede.gitlab.catalog.page.generator.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class House {

    @JsonProperty
    private String name;
    @JsonProperty
    private String personCount;
    @JsonProperty
    private List<Price> prices;

    public String name() {
        return this.name;
    }

    public String personCount() {
        return this.personCount;
    }

    public House setName(final String name) {
        this.name = name;
        return this;
    }

    public House setPersonCount(final String personCount) {
        this.personCount = personCount;
        return this;
    }

    public House addPrice(String season, String price) {

        if (prices == null) {
            prices = new ArrayList<>();
        }

        Price p = new Price()
                .setSeason(season)
                .setPrice(price);

        prices.add(p);

        return this;
    }

    @JsonIgnore
    public String getHeader() {
        final StringBuilder sb = new StringBuilder("Wohnungstyp|");

        prices.forEach(p -> {
            sb.append("|");
            sb.append(p.season());
        });

        sb.append("\n");
        return sb.toString();
    }

    @JsonIgnore
    public String getPrices() {
        final StringBuilder sb = new StringBuilder();

        sb.append(name);
        sb.append("|");
        sb.append(personCount);

        prices.forEach(p -> {
            sb.append("|");
            sb.append(p.price().trim());
        });

        sb.append("\n");

        return sb.toString();

    }


}
