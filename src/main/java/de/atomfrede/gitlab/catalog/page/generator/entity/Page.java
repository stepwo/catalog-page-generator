package de.atomfrede.gitlab.catalog.page.generator.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class Page {

    // ### Allgemein
    @JsonProperty
    private int number;
    @JsonProperty
    private String title;
    @JsonProperty
    private String region;
    @JsonProperty
    private String slogan;
    // ###
    // ### Haus und Region
    @JsonProperty
    private String house;
    // ###
    // Betreuungsangebote
    @JsonProperty
    private String forBabys;
    @JsonProperty
    private String babysExtend;
    @JsonProperty
    private String babysPeriod;

    @JsonProperty
    private String forToddlers;
    @JsonProperty
    private String toddlersExtend;
    @JsonProperty
    private String toddlersPeriod;

    @JsonProperty
    private String forPreschool;
    @JsonProperty
    private String preschoolExtend;
    @JsonProperty
    private String preschoolPeriod;

    @JsonProperty
    private String forChildren;
    @JsonProperty
    private String childrenExtend;
    @JsonProperty
    private String childrenPeriod;

    @JsonProperty
    private String forTeenagers;
    @JsonProperty
    private String teenagerExtend;
    @JsonProperty
    private String teenagerPeriod;

    @JsonProperty
    private String nearby;
    // ###
    // Auf einen Blick
    @JsonProperty
    private String foodAndBeverage;
    @JsonProperty
    private String sourroundings;
    @JsonProperty
    private String sport;
    @JsonProperty
    private String pets;
    @JsonProperty
    private String travel;
    // ###

    // Prices
    @JsonProperty
    private String priceStar;
    @JsonProperty
    private String minimumStay;
    @JsonProperty
    private String priceIncluded;
    @JsonProperty
    private String careOffers;

    // Seasons
    @JsonProperty
    private List<Season> seasons;
    // Houses and Prices
    @JsonProperty
    private List<House> houses;

    public int number() {
        return this.number;
    }

    public String title() {
        return this.title;
    }

    public String region() {
        return this.region;
    }

    public String slogan() {
        return this.slogan;
    }

    public String house() {
        return this.house;
    }

    public String forBabys() {
        return this.forBabys;
    }

    public String babysExtend() {
        return this.babysExtend;
    }

    public String babysPeriod() {
        return this.babysPeriod;
    }

    public String forToddlers() {
        return this.forToddlers;
    }

    public String toddlersExtend() {
        return this.toddlersExtend;
    }

    public String toddlersPeriod() {
        return this.toddlersPeriod;
    }

    public String forPreschool() {
        return this.forPreschool;
    }

    public String preschoolExtend() {
        return this.preschoolExtend;
    }

    public String preschoolPeriod() {
        return this.preschoolPeriod;
    }

    public String forChildren() {
        return this.forChildren;
    }

    public String childrenExtend() {
        return this.childrenExtend;
    }

    public String childrenPeriod() {
        return this.childrenPeriod;
    }

    public String forTeenagers() {
        return this.forTeenagers;
    }

    public String teenagerExtend() {
        return this.teenagerExtend;
    }

    public String teenagerPeriod() {
        return this.teenagerPeriod;
    }

    public String nearby() {
        return this.nearby;
    }

    public String foodAndBeverage() {
        return this.foodAndBeverage;
    }

    public String sourroundings() {
        return this.sourroundings;
    }

    public String sport() {
        return this.sport;
    }

    public String pets() {
        return this.pets;
    }

    public String travel() {
        return this.travel;
    }

    public String priceStar() {
        return this.priceStar;
    }

    public String minimumStay() {
        return this.minimumStay;
    }

    public String priceIncluded() {
        return this.priceIncluded;
    }

    public String careOffers() {
        return this.careOffers;
    }

    public List<Season> seasons() {
        return this.seasons;
    }

    public List<House> houses() {
        return this.houses;
    }

    public Page setNumber(final int number) {
        this.number = number;
        return this;
    }

    public Page setTitle(final String title) {
        this.title = title;
        return this;
    }

    public Page setRegion(final String region) {
        this.region = region;
        return this;
    }

    public Page setSlogan(final String slogan) {
        this.slogan = slogan;
        return this;
    }

    public Page setHouse(final String house) {
        this.house = house;
        return this;
    }

    public Page setForBabys(final String forBabys) {
        this.forBabys = forBabys;
        return this;
    }

    public Page setBabysExtend(final String babysExtend) {
        this.babysExtend = babysExtend;
        return this;
    }

    public Page setBabysPeriod(final String babysPeriod) {
        this.babysPeriod = babysPeriod;
        return this;
    }

    public Page setForToddlers(final String forToddlers) {
        this.forToddlers = forToddlers;
        return this;
    }

    public Page setToddlersExtend(final String toddlersExtend) {
        this.toddlersExtend = toddlersExtend;
        return this;
    }

    public Page setToddlersPeriod(final String toddlersPeriod) {
        this.toddlersPeriod = toddlersPeriod;
        return this;
    }

    public Page setForPreschool(final String forPreschool) {
        this.forPreschool = forPreschool;
        return this;
    }

    public Page setPreschoolExtend(final String preschoolExtend) {
        this.preschoolExtend = preschoolExtend;
        return this;
    }

    public Page setPreschoolPeriod(final String preschoolPeriod) {
        this.preschoolPeriod = preschoolPeriod;
        return this;
    }

    public Page setForChildren(final String forChildren) {
        this.forChildren = forChildren;
        return this;
    }

    public Page setChildrenExtend(final String childrenExtend) {
        this.childrenExtend = childrenExtend;
        return this;
    }

    public Page setChildrenPeriod(final String childrenPeriod) {
        this.childrenPeriod = childrenPeriod;
        return this;
    }

    public Page setForTeenagers(final String forTeenagers) {
        this.forTeenagers = forTeenagers;
        return this;
    }

    public Page setTeenagerExtend(final String teenagerExtend) {
        this.teenagerExtend = teenagerExtend;
        return this;
    }

    public Page setTeenagerPeriod(final String teenagerPeriod) {
        this.teenagerPeriod = teenagerPeriod;
        return this;
    }

    public Page setNearby(final String nearby) {
        this.nearby = nearby;
        return this;
    }

    public Page setFoodAndBeverage(final String foodAndBeverage) {
        this.foodAndBeverage = foodAndBeverage;
        return this;
    }

    public Page setSourroundings(final String sourroundings) {
        this.sourroundings = sourroundings;
        return this;
    }

    public Page setSport(final String sport) {
        this.sport = sport;
        return this;
    }

    public Page setPets(final String pets) {
        this.pets = pets;
        return this;
    }

    public Page setTravel(final String travel) {
        this.travel = travel;
        return this;
    }

    public Page setPriceStar(final String priceStar) {
        this.priceStar = priceStar;
        return this;
    }

    public Page setMinimumStay(final String minimumStay) {
        this.minimumStay = minimumStay;
        return this;
    }

    public Page setPriceIncluded(final String priceIncluded) {
        this.priceIncluded = priceIncluded;
        return this;
    }

    public Page setCareOffers(final String careOffers) {
        this.careOffers = careOffers;
        return this;
    }

    public Page setSeasons(final List<Season> seasons) {
        this.seasons = seasons;
        return this;
    }

    public Page setHouses(final List<House> houses) {
        this.houses = houses;
        return this;
    }


}
